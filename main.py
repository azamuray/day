import json
from typing import Dict

import uvicorn
from fastapi import FastAPI

app = FastAPI()


JSONPATH = "parents.json"


@app.get("/")
def get_all_parents():
    with open(file=JSONPATH, mode="r") as f:
        parents: Dict = json.load(f)

    return parents


@app.get("/{name}")
def get_parent(name: str):
    with open(file=JSONPATH, mode="r") as f:
        parents: Dict = json.load(f)

    parent: Dict = parents.get(name)
    if not parent:
        parent = {name: parent}

    return parent


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, log_level="info")
