FROM python:3.11

WORKDIR /app
ADD . .

RUN pip install -r requirements.txt
EXPOSE 8001/tcp

CMD ["python3", "main.py"]
