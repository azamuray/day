## Deployment

    $ docker build -t day_image .
    $ docker run --name day_cont -dp 0.0.0.0:8001:8000 day_image
